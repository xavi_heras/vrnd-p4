﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	AudioSource source;

	public AudioClip open;
	public AudioClip close;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DoorOpened(){
		Puzzler.Instance.playPoint.GetComponent<Waypoint>().Click(3);
	}
		
	public void PlayOpenSound(){
		source.PlayOneShot (open);
	}

	public void PlayCloseSound(){
		source.PlayOneShot (close);
	}
}
