﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Waypoint : MonoBehaviour
{

	//waypoint light effect
	public GameObject lightColum = null;

	//flag to control light effect visibility
	private bool columnActive = false;

	void Awake()
	{			
		columnActive = false;
		lightColum.SetActive(columnActive);
	}

	public void Enter()
	{		
		//user gazing the waypoint, show particles
		//ShowColumn();
	}
		
	public void Exit()
	{
		//user not watching the waypoint, hide particles
		//HideColumn ();
	}

	public void Click(float speedFactor = 1f)
	{		
		//play waypoint default sound effect
		this.GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
		//make a very quick animation move to the new waypoint
		//Camera.main.transform.DOMove (this.transform.position, .2f, false).SetEase(Ease.Linear);

		float timeToTravel = Vector3.Distance (Puzzler.Instance.player.transform.position, this.transform.position) / 6f;
		timeToTravel *= speedFactor;

		iTween.MoveTo (Puzzler.Instance.player,iTween.Hash("position", new Vector3 (transform.position.x, transform.position.y, transform.position.z), "time", timeToTravel , "easetype", "linear", "onComplete","ReachedWaypoint", "onCompleteTarget", gameObject));

	}

	private void ReachedWaypoint(){
		//update current waypoint
		Puzzler.Instance.MoveToWaypoint(this);		
	}

	public void Hide()
	{		
		this.GetComponentInChildren<SpriteRenderer> ().enabled = false;
		this.GetComponent<BoxCollider> ().enabled = false;
	}
		
	public void Show()
	{
		this.GetComponentInChildren<SpriteRenderer> ().enabled = true;
		this.GetComponent<BoxCollider> ().enabled = true;
	}

	private void ShowColumn()
	{
		columnActive = true;

		lightColum.GetComponent<MeshRenderer> ().material.color = new Color (1, 1, 1, 0);
		DOTween.To(()=> lightColum.GetComponent<MeshRenderer>().material.color, x=> lightColum.GetComponent<MeshRenderer>().material.color = x, new Color (1,1,1,.5f), .5f).SetOptions(true);
		lightColum.SetActive (columnActive);
	}

	private void HideColumn()
	{
		columnActive = false;
		lightColum.SetActive (columnActive);
		DOTween.Kill (lightColum.GetComponent<MeshRenderer> ().material.color, false);
	}
}
