﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Book : MonoBehaviour {

	float amount = 1f;
	public CanvasGroup canvas;
	private bool clicked =false;
	AudioSource source;
	public AudioClip dissolve;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Click(){
		//fade text 

		//fade book
		//this.GetComponent<MeshRenderer> ().material.SetFloat ("_Progress", .5F);

		//Puzzler.Instance.GoToDoor ();

		//this.GetComponent<MeshRenderer>().material


		if (!clicked) {
			clicked = true;
			source.PlayOneShot (dissolve);
			Tweener tweener = DOTween.To (() => amount, x => amount = x, 0f, 2f);
			tweener.OnUpdate (() => {
				this.GetComponent<MeshRenderer> ().material.SetFloat ("_Progress", amount);
				canvas.alpha = amount / 10;
			});
			tweener.OnComplete (() => {
				Destroy (this.gameObject);
				Puzzler.Instance.doorPoint.GetComponent<Waypoint>().Click(3);
			});
		}
	
	}


}
