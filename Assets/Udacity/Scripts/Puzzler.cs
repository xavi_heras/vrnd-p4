﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

//Singleton class to control the puzzle
public class Puzzler : MonoBehaviourSingleton<Puzzler> {

#if UNITY_EDITOR
	//development variables
	public bool startFromBeginning = false;
	public Texture2D nocursor;



#endif

	//variable with the current waypoint
	public Waypoint currentPoint = null;

	//fade in- fade out image
	public Image fade;

	//flag to control reload 
	private bool reloading = false;


	public GameObject player;
	public GameObject eventSystem;
	public GameObject startUI, restartUI;
	public GameObject startPoint, doorPoint, playPoint, exitPoint, portalPoint;
	public GameObject[] puzzleSpheres; //An array to hold our puzzle spheres

	public int puzzleLength = 5; //How many times we light up.  This is the difficulty factor.  The longer it is the more you have to memorize in-game.
	public float puzzleSpeed = 1f; //How many seconds between puzzle display pulses
	public int[] puzzleOrder; //For now let's have 5 orbs

	private int currentDisplayIndex = 0; //Temporary variable for storing the index when displaying the pattern
	public bool currentlyDisplayingPattern = true;
	public bool playerWon = false;

	public int currentSolveIndex = 0; //Temporary variable for storing the index that the player is solving for in the pattern.

	public bool isDoorOpened = false;
	public GameObject door;


	public GameObject reticle;

	public GameObject candlesParticles;
	public GameObject portal;
	public GameObject portalParticles;
	public GameObject portalView;
	public GameObject bookExit;

	// Use this for initialization
	void Start () {
		
#if UNITY_EDITOR


		//Cursor.SetCursor (nocursor, Vector2.zero, CursorMode.Auto);
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;

		if (startFromBeginning) {
			Camera.main.transform.position = startPoint.transform.position;
			currentPoint = startPoint.GetComponent<Waypoint> ();
			MoveToWaypoint (currentPoint);
		}
#endif
		eventSystem.SetActive (false);
		//force fade initial color/alpha
		if (fade != null) {
			fade.color = new Color (fade.color.r, fade.color.g, fade.color.b, 1);
			DOTween.To(()=> fade.color, x=> fade.color = x, new Color (fade.color.r,fade.color.g,fade.color.b,0), 2f).SetOptions(true).OnComplete(StartGame);
		}

		puzzleOrder = new int[puzzleLength]; //Set the size of our array to the declared puzzle length
		generatePuzzleSequence (); //Generate the puzzle sequence for this playthrough.  
		//hide portal/view
		portal.SetActive (false);
		portalParticles.SetActive (false);
		portalView.SetActive (false);

		ResetOrbsPositions();

	}
		
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
//development input
		if (Input.GetKeyDown(KeyCode.Alpha1)) Puzzler.Instance.startPoint.GetComponent<Waypoint>().Click();
		else if (Input.GetKeyDown(KeyCode.Alpha2)) Puzzler.Instance.doorPoint.GetComponent<Waypoint>().Click();
		else if (Input.GetKeyDown(KeyCode.Alpha3)) Puzzler.Instance.playPoint.GetComponent<Waypoint>().Click();
		else if (Input.GetKeyDown(KeyCode.Alpha4)) Puzzler.Instance.exitPoint.GetComponent<Waypoint>().Click(.1f);
		else if (Input.GetKeyDown(KeyCode.Alpha5)) Puzzler.Instance.portalPoint.GetComponent<Waypoint>().Click(.1f);
		else if (Input.GetKeyDown(KeyCode.S)) startPuzzle(puzzleSpeed);
		else if (Input.GetKeyDown(KeyCode.R)) Reload(3);
		else if (Input.GetKeyDown(KeyCode.I)) FadeIn(1.5f);
		else if (Input.GetKeyDown(KeyCode.O)) FadeOut(1.5f);
		else if (Input.GetKeyDown(KeyCode.X)) StartCoroutine(AnimateOrbsIn());
		else if (Input.GetKeyDown(KeyCode.C)) StartCoroutine(puzzleSuccess());
#endif
	}


	public void MoveToWaypoint (Waypoint point){
		Debug.Log ("MoveToWaypoint " + point.name);
		//update current waypoint and 
		if (currentPoint == null)
			currentPoint = point;
	
		//if the user moves to a new waypoint, show last one and hide current one
		if (point != currentPoint) {
			currentPoint.Show ();
			currentPoint = point;
			currentPoint.Hide ();
		}
	
		//Special behaviours 

		switch (point.name) {
			case "WaypointDoor":
				if (!isDoorOpened){
					door.GetComponent<Animation> ().Play ("Open_Anim");
					door.GetComponent<Door> ().PlayOpenSound ();
					isDoorOpened=true;					
				}

				break;
			case "WaypointPlay":
				if (isDoorOpened) {
					door.GetComponent<Animation> ().Play ("Close_Anim");
					door.GetComponent<Door> ().PlayCloseSound ();
					isDoorOpened = false;					
				}
				StartCoroutine(AnimateOrbsIn());
				break;
		}

	}


	private void ResetOrbsPositions(){
		for (int i = 0; i < puzzleSpheres.Length; i++) {
			//puzzleSpheres [i].transform.position = (Vector3) iTweenPath.GetPath (puzzleSpheres [i].name + "In").GetValue (0);
			puzzleSpheres [i].AddComponent <FloatingScript> ();
			puzzleSpheres [i].GetComponent<FloatingScript>().floatingStrength = new Vector2 (0f, Random.Range(-.001f, .001f));
		}
	}

	private IEnumerator AnimateOrbsIn(){
		float time;
		time = 1f;
		/*
		for (int i = 0; i < puzzleSpheres.Length; i++) {
			MoveOrbPath (puzzleSpheres [i], "In", time);

		}
		*/
		yield return new WaitForSeconds (time);

		startPuzzle (puzzleSpeed);
	}

	private void MoveOrbPath (GameObject orb, string name, float time){
		Debug.Log (orb.name + name);
		iTween.MoveTo(orb, iTween.Hash("path",iTweenPath.GetPath(orb.name + name),
			"easetype", "linear",
			"time", time));
		
	}

	private void StartGame(){
		eventSystem.SetActive (true);
	}

	private void FadeIn(float time = 1.5f)
	{
		DOTween.To(()=> fade.color, x=> fade.color = x, new Color (fade.color.r,fade.color.g,fade.color.b,1), time).SetOptions(true);
	}

	private void FadeOut(float time = 1.5f)
	{		
		DOTween.To(()=> fade.color, x=> fade.color = x, new Color (fade.color.r,fade.color.g,fade.color.b,0), time).SetOptions(true);
	}

	public void Reload(float time){
		if (!reloading) {
			reloading = true;
			FadeIn (time);
			StartCoroutine (FadeAndReload (time));
		}
	}

	public IEnumerator FadeAndReload(float time){
		yield return new WaitForSeconds (time);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}


	public void GoToDoor(){
		
		iTween.MoveTo(player, iTween.Hash("path",iTweenPath.GetPath("pathToDoor"),
									      "easetype", "linear",
										  "time", 5));

	}
		
	public void playerSelection(GameObject sphere) {
		if(playerWon != true) { //If the player hasn't won yet
			int selectedIndex=0;
			//Get the index of the selected object
			for (int i = 0; i < puzzleSpheres.Length; i++) { //Go through the puzzlespheres array
				if(puzzleSpheres[i] == sphere) { //If the object we have matches this index, we're good
					Debug.Log("Looks like we hit sphere: " + i);
					selectedIndex = i;
				}
			}
			solutionCheck (selectedIndex);//Check if it's correct
		}
	}

	public bool checkCurrentSelection (GameObject sphere) {
		bool ret = false;

		if (puzzleSpheres [puzzleOrder [currentSolveIndex]] == sphere) {
			ret = true;
			//currentSolveIndex++;
		} else {			
			StartCoroutine (puzzleFailure ());
		}

		return ret;
	}

	public void solutionCheck(int playerSelectionIndex) { //We check whether or not the passed index matches the solution index
		if (playerSelectionIndex == puzzleOrder [currentSolveIndex]) { //Check if the index of the object the player passed is the same as the current solve index in our solution array
			currentSolveIndex++;
			Debug.Log ("Correct!  You've solved " + currentSolveIndex + " out of " + puzzleLength);
			if (currentSolveIndex >= puzzleLength) {
				StartCoroutine( puzzleSuccess ());
			}
		} else {
			puzzleFailure ();
		}

	}

	public void startPuzzle(float speed) { //Begin the puzzle sequence
		Debug.Log ("StartPuzzle");
		//Generate a random number one through five, save it in an array.  Do this n times.
		//Step through the array for displaying the puzzle, and checking puzzle failure or success.
	//	startUI.SetActive (false);
		eventSystem.SetActive(false);
		reticle.SetActive (false);

		//iTween.MoveTo (player, playPoint.transform.position, 5f);

		CancelInvoke ("displayPattern");
		InvokeRepeating("displayPattern", 1, speed); //Start running through the displaypattern function
		currentSolveIndex = 0; //Set our puzzle index at 0

	}

	void displayPattern() { //Invoked repeating.
		currentlyDisplayingPattern = true; //Let us know were displaying the pattern
		eventSystem.SetActive(false); //Disable gaze input events while we are displaying the pattern.
		reticle.SetActive (false);

		if (currentlyDisplayingPattern == true) { //If we are not finished displaying the pattern
			if (currentDisplayIndex < puzzleOrder.Length) { //If we haven't reached the end of the puzzle
				Debug.Log (puzzleOrder[currentDisplayIndex] + " at index: " + currentDisplayIndex); 
					puzzleSpheres [puzzleOrder [currentDisplayIndex]].GetComponent<Orb> ().patternLightUp (puzzleSpeed); //Light up the sphere at the proper index.  For now we keep it lit up the same amount of time as our interval, but could adjust this to be less.
				currentDisplayIndex++; //Move one further up.
			} else {
				Debug.Log ("End of puzzle display");
				currentlyDisplayingPattern = false; //Let us know were done displaying the pattern
				currentDisplayIndex = 0;
				CancelInvoke(); //Stop the pattern display.  May be better to use coroutines for this but oh well
				eventSystem.SetActive(true); //Enable gaze input when we aren't displaying the pattern.
				reticle.SetActive (true);
			}
		}
	}


	public void generatePuzzleSequence() {

		int tempReference;
		for (int i = 0; i < puzzleLength; i++) { //Do this as many times as necessary for puzzle length
			tempReference = Random.Range(0, puzzleSpheres.Length); //Generate a random reference number for our puzzle spheres
			puzzleOrder [i] = tempReference; //Set the current index to our randomly generated reference number
		}
	}


	public void resetPuzzle() { //Reset the puzzle sequence

		reticle.SetActive (false);
		StartCoroutine (FadeAndLoad ());


		//Debug.Log ("Reset puzzle");

		//SceneManager.LoadScene (SceneManager.GetActiveScene ().name);

		/*
        iTween.MoveTo (player, 
            iTween.Hash (
                "position", startPoint.transform.position, 
                "time", 4, 
                "easetype", "linear",
                "oncomplete", "resetGame", 
                "oncompletetarget", this.gameObject
            )
        );
		*/

		//restartUI.SetActive (false);
	}
	public void resetGame() {
		//restartUI.SetActive (false);
		//startUI.SetActive (true);
		Debug.Log ("resetGame");
		//playerWon = false;
		//generatePuzzleSequence (); //Generate the puzzle sequence for this playthrough.  
	}

	public IEnumerator FadeAndLoad(){
		Puzzler.Instance.portalPoint.GetComponent<Waypoint>().Click(10f);
		DOTween.To (() => fade.color, x => fade.color = x, new Color (fade.color.r, fade.color.g, fade.color.b, 1), 6).SetOptions (true);
		yield return new WaitForSeconds(6 + .5f);
		SceneManager.LoadScene ("Main");
	}

	public IEnumerator puzzleFailure() { //Do this when the player gets it wrong
		Debug.Log("You've Failed, Resetting puzzle");

		for (int i = 0; i < puzzleSpheres.Length; i++) { 
			//puzzleSpheres [i].GetComponent<Orb> ().patternLightUpFailure (puzzleSpeed/10); 
		}

		currentSolveIndex = 0;

		yield return new WaitForSeconds(.1f);

		startPuzzle (puzzleSpeed);

	}

	public IEnumerator puzzleSuccess() { //Do this when the player gets it right

		reticle.SetActive (false);

		yield return new WaitForSeconds(2f);


		ParticleSystem ps = portalParticles.GetComponent<ParticleSystem>();
		var main = ps.main;
		ParticleSystem.MinMaxGradient c = main.startColor;

		for (int i = 0; i < puzzleSpheres.Length; i++) {
			puzzleSpheres [i].GetComponent<AudioSource> ().Stop ();
			Destroy (puzzleSpheres [i].GetComponent<FloatingScript> ());
			puzzleSpheres [i].GetComponent<Orb> ().OrbColor (c);

			iTween.MoveTo (puzzleSpheres [i], 
				iTween.Hash (
					"position", portal.transform.position, 
					"time", .2f, 
					"easetype", "linear"
				)
			);
		}

		yield return new WaitForSeconds(1.5f);

		//hide spheres
		for (int i = 0; i < puzzleSpheres.Length; i++) { 
			Destroy (puzzleSpheres [i]);
			//puzzleSpheres [i].GetComponent<Orb> ().DisableCollider ();
			//puzzleSpheres [i].GetComponent<Orb> ().Enter ();
		}

		candlesParticles.SetActive (false);
		//animate spheres
		Vector3 finalScale = portal.transform.localScale;
		float portalScaleTime = 3f;

		portal.transform.localScale = Vector3.zero;
		portal.SetActive (true);
		iTween.ScaleTo (portal, iTween.Hash ("scale", finalScale, "time", portalScaleTime, "easType", iTween.EaseType.easeInOutBounce));


		finalScale = portalParticles.transform.localScale;
		portalParticles.transform.localScale = Vector3.zero;
		portalParticles.SetActive (true);
		iTween.ScaleTo (portalParticles, iTween.Hash ("scale", finalScale, "time", portalScaleTime, "easType", iTween.EaseType.easeInOutBounce));

		portalView.SetActive (true);
		bookExit.SetActive (true);
		bookExit.GetComponent<BookExit> ().Appear ();

		Puzzler.Instance.exitPoint.GetComponent<Waypoint>().Click(6f);




		Debug.Log ("puzzleSuccess");

		yield return new WaitForSeconds(1f);

		reticle.SetActive (true);

		/*
        iTween.MoveTo (player, 
            iTween.Hash (
                "position", restartPoint.transform.position, 
                "time", 2, 
                "easetype", "linear",
                "oncomplete", "finishingFlourish", 
                "oncompletetarget", this.gameObject
            )
        );
        */
	}

	public void finishingFlourish() { //A nice visual flourish when the player wins
		//this.GetComponent<AudioSource>().Play(); //Play the success audio
		restartUI.SetActive (true);
		playerWon = true;

	}

}
