﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour {

	public GameObject glow;
	public GameObject circle;
	public GameObject dust;
	public GameObject orb;
	public GameObject smoke;
	public GameObject smallparticles;

	public Color failure;

	AudioSource source;

	public AudioClip gaze;
	public AudioClip select;
	public AudioClip fail;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		//circle.SetActive (false);
		//dust.SetActive (false);
		//smoke.SetActive (false);
		//smallparticles.SetActive (false);
		aestheticReset ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Enter()
	{
		patternLightUp ();
		source.PlayOneShot (gaze);
	}

	public void Exit()
	{
		aestheticReset ();
		if (source.clip == gaze) {
			source.Stop ();
		}
	}

	public void Click()
	{		
		Debug.Log ("orb click");


		if (Puzzler.Instance.checkCurrentSelection (gameObject)) {
			Debug.Log ("orb OK");
			source.PlayOneShot (select);
			circle.GetComponent<ParticleSystem> ().Play ();
			dust.GetComponent<ParticleSystem> ().Play ();
		} else {
			iTween.ShakePosition (gameObject, iTween.Hash ("x", .05f, "y", .05f, "time", Puzzler.Instance.puzzleSpeed/3)); 
			source.PlayOneShot (fail);

			Debug.Log ("orb KO");
		}

		StartCoroutine(playerSelection ());
	}

	public void DisableCollider(){
		this.GetComponent<SphereCollider> ().enabled = false;
	}

	public void patternLightUp(float duration) { //The lightup behavior when displaying the pattern
		StartCoroutine(lightFor(duration));
	}

	public void patternLightUpFailure(float duration) { //The lightup behavior when failing
		
		StartCoroutine(lightFailureFor(duration));

	}


	public IEnumerator playerSelection() {
		
		yield return new WaitForSeconds(Puzzler.Instance.puzzleSpeed/3  -.1f);
		Puzzler.Instance.playerSelection(this.gameObject);
		//this.GetComponent<GvrAudioSource>().Play();
	}

	public void aestheticReset() {
		//this.GetComponent<MeshRenderer>().material = defaultMaterial; //Revert to the default material
		//this.GetComponentInChildren<ParticleSystem>().enableEmission = false; //Turn off particle emission

		//circle.SetActive (false);
		//dust.SetActive (false);
		//smoke.SetActive (false);
		//smallparticles.SetActive (false);

		//circle.GetComponent<ParticleSystem> ().Stop ();
		//dust.GetComponent<ParticleSystem>().Stop ();
		smoke.GetComponent<ParticleSystem>().Stop ();
		smallparticles.GetComponent<ParticleSystem>().Stop ();


	}

	public void patternLightUp() { //Lightup behavior when the pattern shows.
		//this.GetComponent<MeshRenderer>().material = lightUpMaterial; //Assign the hover material
		//this.GetComponentInChildren<ParticleSystem>().enableEmission = true; //Turn on particle emmission
		//this.GetComponent<GvrAudioSource> ().Play (); //Play the audio attached
		//circle.GetComponent<ParticleSystem>().Play();
		//dust.GetComponent<ParticleSystem>().Play();
		smoke.GetComponent<ParticleSystem>().Play();
		smallparticles.GetComponent<ParticleSystem>().Play();

		//circle.SetActive (true);
		//dust.SetActive (true);
		//smoke.SetActive (true);
		//smallparticles.SetActive (true);

	}


	IEnumerator lightFor(float duration) { //Light us up for a duration.  Used during the pattern display
		//patternLightUp ();
		source.PlayOneShot(select);
		circle.GetComponent<ParticleSystem>().Play();
		dust.GetComponent<ParticleSystem>().Play();
		yield return new WaitForSeconds(duration-.1f);

		//aestheticReset ();
	}

	IEnumerator lightFailureFor(float duration) { //Light us up for a duration.  Used during the pattern display
		//patternLightUp ();
		ParticleSystem ps = orb.GetComponent<ParticleSystem>();
		var main = ps.main;
		ParticleSystem.MinMaxGradient original = main.startColor;
			
		main.startColor = new ParticleSystem.MinMaxGradient (failure);

		yield return new WaitForSeconds(duration-.1f);

		main.startColor = original;
		//aestheticReset ();
	}


	public void OrbColor (ParticleSystem.MinMaxGradient c){
		ParticleSystem ps = orb.GetComponent<ParticleSystem>();
		var main = ps.main;
		main.startColor = c;

		ps = glow.GetComponent<ParticleSystem>();
		main = ps.main;
		main.startColor = c;

	}
}
