﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Main : MonoBehaviour {

	public Image fade;
	public string game;
	public float fadeTime = 2f;
	public GameObject reticle;
	public GameObject[] puzzleSpheres;

	// Use this for initialization
	void Start () {

		for (int i = 0; i < puzzleSpheres.Length; i++) {
			
			puzzleSpheres [i].AddComponent <FloatingScript> ();
			puzzleSpheres [i].GetComponent<FloatingScript>().floatingStrength = new Vector2 (0f, Random.Range(-.002f, .002f));

		}


		fade.color = Color.black;
		DOTween.To (() => fade.color, x => fade.color = x, new Color (fade.color.r, fade.color.g, fade.color.b, 0), fadeTime*2).SetOptions (true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartGame(){		
		reticle.SetActive (false);
		StartCoroutine (FadeAndLoad());
	}

	public IEnumerator FadeAndLoad(){
		DOTween.To (() => fade.color, x => fade.color = x, new Color (fade.color.r, fade.color.g, fade.color.b, 1), fadeTime).SetOptions (true);
		yield return new WaitForSeconds(fadeTime + .5f);
		SceneManager.LoadScene (game);
	}
}
