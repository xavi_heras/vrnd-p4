﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BookExit : MonoBehaviour {

	float amount = 1f;
	public CanvasGroup canvas;

	// Use this for initialization
	void Start () {
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Progress", 0);
		canvas.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Appear(){
		amount = 0;
		Tweener tweener = DOTween.To (() => amount, x => amount = x, 1f, 3f);
		tweener.OnUpdate (() => {
			this.GetComponent<MeshRenderer> ().material.SetFloat ("_Progress", amount);
			canvas.alpha = amount;
		});
		tweener.OnComplete (() => {

		});

	}
}
