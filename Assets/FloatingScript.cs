﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingScript : MonoBehaviour {

	public Vector2 floatingStrength;


	void Update () {
		this.transform.position = new Vector3 (this.transform.position.x + (Mathf.Sin (Time.time) * floatingStrength.x), this.transform.position.y + (Mathf.Sin (Time.time) * floatingStrength.y), this.transform.position.z);
	}
}
